module Mahou.Types 
  (MahouExpression (..),
   MahouEvalState (..),
   MahouVerbs (..),
   MahouStack (..),
   MahouState (..),
   MahouEntry (..),
   module Control.Monad.State,
   module Network.CGI)
 where

import Control.Monad.State
import Network.CGI
import Data.Map as Map

data MahouExpression =
   MahouInteger Integer 
 | MahouDouble Double
 | MahouString String
 | MahouList [MahouExpression]
 | MahouDef String [MahouExpression]
 | MahouVerb String
 | MahouTaggedVerb String
 | MahouMemberGet String
 | MahouMemberSet String
 | MahouOperator String
 | MahouError String
 | MahouCharacter Char
 | MahouObject (Map.Map String [MahouExpression])
 | MahouLock [[MahouExpression]]
 deriving (Show,Read,Eq)


data MahouEntry =  MahouBuiltin (MahouEvalState)
                 | MahouUser [MahouExpression]
data MahouVerbs = MahouVerbs (Map.Map String MahouEntry)
  deriving (Show)
type MahouStack = [MahouExpression]
type MahouState = (MahouVerbs , MahouStack, String, MahouStack)
type MahouEvalState = StateT MahouState (CGIT IO) ()

instance Show MahouEntry where
  show (MahouBuiltin _) = "BUILTIN"
  show (MahouUser x) = "{" ++ show x ++ "}"

