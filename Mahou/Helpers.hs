module Mahou.Helpers
  (unescape,
   unescape2,
   toInt,
   insertAt,
   removeAt,
   setAt)
 where

import Data.List
import Data.List.Split
import Data.Digits
import Data.Maybe
import Data.Ord

unescape ('\\':'\\':xs) = '\\' : unescape xs
unescape ('\\':'n':xs) = '\n' : unescape xs
unescape ('\\':'t':xs) = '\t' : unescape xs
unescape ('\\':'r':xs) = '\r' : unescape xs
unescape ('\\':'\'':xs) = '\"' : unescape xs
unescape (x:xs) = x : unescape xs
unescape [] = []

unescape2 ('{':'{':xs) = '{' : unescape2 xs
unescape2 ('{':'\\':xs) = '}' : unescape2 xs
unescape2 (x:xs) = x : unescape2 xs
unescape2 [] = []

toInt :: Integer -> Int
toInt = fromIntegral

insertAt n e xs = let (a,b) = splitAt (fromIntegral n) xs in (a ++ [e]) ++ b

removeAt n xs = let (a,b) = splitAt (fromIntegral n) xs in a ++ (tail b)

setAt n e xs = let (a,b) = splitAt (fromIntegral n) xs in a ++ (e : tail b)
