module Mahou.Eval
  (eval, initEvalState, runMain)
 where

import Mahou.Types
import Mahou.Helpers
import Mahou.Parser

import Data.Map as Map hiding (map)
import Data.List hiding (map)
import Data.Char
import Network.URI
import System.IO
import System.Exit
import Numeric

{-
  Everything that can easily be written in Mahou itself should be implemented not as a built-in but as a function
  in std.mh. This helps keeping the interpreter as simple as possible. 
-}

runMain :: [MahouExpression] -> MahouEvalState
runMain prg = do
  initEvalState
  prepare prg
  execVerb "main"

prepare :: [MahouExpression] -> MahouEvalState
prepare ((MahouDef name code) : xs) = do
  (MahouVerbs v, s, h, t) <- get
  let v' = Map.insert name (MahouUser code) v
  put (MahouVerbs v', s, h, t)
  prepare xs
prepare (x : xs) = prepare xs
prepare [] = return ()
  
eval (a : MahouOperator ">" : b : xs) = do
  eval $ a : MahouVerb "dup" : b : MahouVerb "swap" : xs
eval (MahouOperator "^" : b : xs) = do
  eval $ MahouVerb "swap" : b : MahouVerb "swap" : xs
eval (MahouVerb verb : xs) = execVerb verb >> eval xs
eval (MahouMemberGet mem : xs) = pushStack (MahouString mem) >> eval ((MahouVerb "get-property"):xs)
eval (MahouMemberSet mem : xs) = pushStack (MahouString mem) >> m_swap >> eval ((MahouVerb "set-property"):xs)
eval (MahouTaggedVerb verb : xs) = do
  pushStack (MahouString "@") >> m_getProperty 
  st <- getStack
  case st of
    (MahouString tag:ss) -> putStack ss >> (eval $ (MahouVerb $ tag ++ "-" ++ verb) : xs)
    _ -> pushStack (MahouError "- @ - Invalid!") >> eval xs
eval (q : xs) = (pushStack $ q) >> eval xs
eval [] = return ()

getStack :: StateT MahouState (CGIT IO)  MahouStack
getStack = do
  (_, s, _, t) <- get
  return s

putStack :: MahouStack -> MahouEvalState
putStack s = do
  (v, _, h, t) <- get
  put (v, s, h, t)

pushStack :: MahouExpression -> MahouEvalState
pushStack e = do
  s <- getStack
  putStack $ e : s

putHtml :: String -> MahouEvalState
putHtml n =  do
  (v, s, h, t) <- get
  put (v, s, h ++ n, t)

execVerb :: String -> MahouEvalState
execVerb verb = do
  (MahouVerbs v, _, _, t) <- get
  let lkp = Map.lookup verb v
  case lkp of
    Just (MahouBuiltin q) -> q
    Just (MahouUser q) -> eval q
    Nothing -> error $ "- ERROR - Verb `" ++ verb ++ "' does not exist!"

initEvalState :: MahouEvalState
initEvalState = do
  s <- getStack
  put (MahouVerbs $ fromList 
    [("pushl", MahouBuiltin m_pushl),
     ("popl", MahouBuiltin m_popl),
     ("dell", MahouBuiltin m_dell),
     ("peekl", MahouBuiltin m_peekl),
     ("newl", MahouBuiltin m_newl),
     ("getl", MahouBuiltin m_getl),
     ("setl", MahouBuiltin m_setl),
     ("nop", MahouBuiltin m_nop),
     ("cry", MahouBuiltin m_cry),
     ("add", MahouBuiltin m_add),
     ("append", MahouBuiltin m_append),
     ("as-str", MahouBuiltin m_asStr),
     ("assert", MahouBuiltin m_assert),
     ("at", MahouBuiltin m_at),
     ("box", MahouBuiltin m_box),
     ("breakpoint", MahouBuiltin m_breakpoint),
     ("cnat", MahouBuiltin m_cnat),
     ("debug-stack", MahouBuiltin m_debugStack),
     ("define", MahouBuiltin m_define),
     ("div", MahouBuiltin m_div),
     ("drop", MahouBuiltin m_drop),
     ("dup", MahouBuiltin m_dup),
     ("echo", MahouBuiltin m_echo),
     ("equal", MahouBuiltin m_equal),
     ("eval", MahouBuiltin m_eval),
     ("explode", MahouBuiltin m_explode),
     ("filter", MahouBuiltin m_filter),
     ("find-index", MahouBuiltin m_findIndex),
     ("get-cookie", MahouBuiltin m_getCookie),
     ("get-element", MahouBuiltin m_getElement),
     ("get-property", MahouBuiltin m_getProperty),
     ("get-query-uri", MahouBuiltin m_getQueryURI),
     ("head", MahouBuiltin m_head),
     ("is-space", MahouBuiltin m_isSpace), 
     ("is-number", MahouBuiltin m_isNumber),
     ("is-alpha-num", MahouBuiltin m_isAlphaNum),
     ("is-alpha", MahouBuiltin m_isAlpha),
     ("length", MahouBuiltin m_length),
     ("map", MahouBuiltin m_map),
     ("mod", MahouBuiltin m_mod),
     ("mul", MahouBuiltin m_mul),
     ("not", MahouBuiltin m_not),
     ("pop", MahouBuiltin m_pop),
     ("prepend", MahouBuiltin m_prepend),
     ("put-str", MahouBuiltin m_putStr),
     ("put-str-ln", MahouBuiltin m_putStrLn),
     ("range", MahouBuiltin m_range),
     ("read-file", MahouBuiltin m_readFile),
     ("read-int", MahouBuiltin m_readInt),
     ("reduce", MahouBuiltin m_reduce),
     ("replicate", MahouBuiltin m_replicate),
     ("reverse", MahouBuiltin m_reverse),
     ("rotate-stack", MahouBuiltin m_rotateStack),
     ("rotate-stack.", MahouBuiltin m_rotateStack2),
     ("rotate-stack-top", MahouBuiltin m_rotateStackTop),
     ("rotate-stack-top.", MahouBuiltin m_rotateStackTop2),
     ("set-cookie", MahouBuiltin m_setCookie),
     ("set-property", MahouBuiltin m_setProperty),
     ("show", MahouBuiltin m_show),
     ("show-ln", MahouBuiltin m_showLn),
     ("signum", MahouBuiltin m_signum),
     ("sub", MahouBuiltin m_sub),
     ("swap", MahouBuiltin m_swap),
     ("tail", MahouBuiltin m_tail),
     ("take", MahouBuiltin m_take),
     ("uncons", MahouBuiltin m_uncons)
    ] , s, "", [])

stdStr (MahouOperator o) = o
stdStr (MahouError s) = "ERROR: " ++ s
stdStr (MahouInteger i) = show i
stdStr (MahouDouble d) = showFFloat (Nothing) d ""
stdStr (MahouString s) = show s
stdStr (MahouCharacter c) = '\'' : [c]
stdStr (MahouVerb v) = v
stdStr (MahouList xs) =
  "[" ++ (intercalate ", " $ stdStr' (MahouList xs)) ++ "]"
 where stdStr' (MahouList []) = []
       stdStr' (MahouList (x : xs)) = stdStr x : stdStr' (MahouList xs)

m_nop :: MahouEvalState
m_nop = return ()

m_newl :: MahouEvalState
m_newl = do
  (a, b, c, t) <- get
  put (a, b, c, (MahouObject (fromList []) : t))
  
m_dell = m_popl >> m_pop
  
m_getl :: MahouEvalState
m_getl = do
 st <- getStack
 case st of
   (MahouString s : xs) -> do
     putStack xs
     m_peekl
     pushStack (MahouString s)
     m_getProperty
     m_swap
     m_pop
   _ -> pushStack $ MahouError "- getl - Invalid arguments!"
   
m_setl :: MahouEvalState
m_setl = do
 st <- getStack
 case st of
   (MahouString s : val : xs) -> do
     putStack xs
     m_popl
     pushStack (MahouString s)
     pushStack val
     m_setProperty
     m_pushl
   _ -> pushStack $ MahouError "- setl - Invalid arguments!"

m_cry :: MahouEvalState
m_cry = do 
  pushStack . MahouList . map MahouInteger $ [1..]
-----
-----

m_add :: MahouEvalState
m_add = do
  st <- getStack
  case st of
    (MahouInteger b : MahouInteger a : xs) -> putStack $ (MahouInteger $ a + b) : xs
    (MahouDouble b : MahouDouble a : xs) -> putStack $ (MahouDouble $ a + b) : xs
    (MahouInteger b : MahouDouble a : xs) -> putStack $ (MahouDouble $ a + (fromIntegral b)) : xs
    (MahouDouble b : MahouInteger a : xs) -> putStack $ (MahouDouble $ (fromIntegral a) + b) : xs
    _ -> pushStack $ MahouError "- add - Invalid arguments!"

m_append :: MahouEvalState
m_append = do
  st <- getStack
  case st of
    (b : MahouList a : xs) -> putStack $ (MahouList $ a ++ [b]) : xs
    (MahouCharacter b : MahouString a : xs) -> putStack $ (MahouString $ a ++ [b]) : xs
    (MahouString b : MahouCharacter a : xs) -> putStack $ (MahouString $ [a] ++ b) : xs
    _ -> m_cnat

m_asStr :: MahouEvalState
m_asStr = do
  st <- getStack
  case st of 
    (q : xs) -> putStack $ (MahouString . stdStr $ q) : xs
    _ -> pushStack $ MahouError "- as-str - Invalid arguments!"

m_assert :: MahouEvalState
m_assert = do
  st <- getStack
  case st of
    (MahouInteger a : MahouInteger 1 : xs) -> putStack xs
    (MahouInteger a : xs) -> error $ "- assert - ASSERTION FAILED: No.: " ++ show a
    _ -> error $ "- assert - FATAL!"
    
m_at :: MahouEvalState
m_at = do
  st <- getStack
  case st of
    (MahouInteger a : xs) -> putStack $ (xs !! (toInt a)) : xs
    _ -> error $ "- at - Invalid arguments!"

m_box :: MahouEvalState
m_box = do
  st <- getStack
  case st of
    (x : xs) -> putStack $ (MahouList [x]) : xs
    _ -> pushStack $ MahouError "- box - Invalid arguments!"

m_breakpoint :: MahouEvalState
m_breakpoint = do
  (v, st, h, t) <- get
  liftIO $ putStrLn ""
  liftIO $ putStr "debug: "
  liftIO $ hFlush stdout
  ln <- liftIO $ getLine
  case words ln of
    ["{}st"] -> do case st of
                    (a:as) -> do liftIO $ print a
                   m_breakpoint
    ["{}s"] -> do liftIO $ print st
                  m_breakpoint
    ["{}sm"] -> do liftIO . print . take 5 $ st
                   m_breakpoint
    ["{}sn", num] -> do liftIO . print . take (read num) $ st
                        m_breakpoint
    ["{}si", num] -> do liftIO . print . (!! (read num)) $ st
                        m_breakpoint
    ["{}v"] -> do liftIO . print $ v
                  m_breakpoint
    ["{}e"] -> do liftIO $ exitFailure
    ["{}c"] -> do return ()
    ["{}p", p] -> do let pp = runParserWithString parseData p
                     pushStack $ pp
                     m_breakpoint
    [q] -> do execVerb q
              m_breakpoint
    _ -> do liftIO $ putStrLn "Unknown command"

m_cnat :: MahouEvalState
m_cnat = do
  st <- getStack
  case st of
    (MahouString b : MahouString a : xs) -> putStack $ (MahouString $ a ++ b) : xs
    (MahouString b : MahouCharacter a : xs) -> putStack $ (MahouString $ a : b) : xs
    (MahouCharacter b : MahouString a : xs) -> putStack $ (MahouString $ a ++ [b]) : xs
    (MahouCharacter b : MahouCharacter a :xs) -> putStack $ (MahouString $ a : b : []) : xs
    (MahouInteger b : MahouInteger a : xs) -> do
      let as = show . abs $ a
      let bs = show . abs $ b
      putStack $ (MahouInteger . read $ as ++ bs) : xs
    (MahouList b : MahouList a : xs) -> putStack $ (MahouList $ a ++ b) : xs
    (MahouString b : a : xs) -> putStack $ (MahouString $ stdStr a ++ b) : xs
    (b : MahouString a : xs) -> putStack $ (MahouString $ a ++ stdStr b) : xs
    _ -> pushStack $ MahouError "- cnat - Invalid arguments!"

m_debugStack :: MahouEvalState
m_debugStack = do
  st <- getStack
  liftIO $ print st

m_define :: MahouEvalState
m_define = do
  st <- getStack
  case st of
    (MahouString name : code : xs) -> do 
      (MahouVerbs v, s, h, t) <- get
      let v' = Map.insert name (MahouUser [code]) v
      put (MahouVerbs v', xs, h, t)
    _ -> pushStack $ MahouError "- define - Stack is empty!"

m_div :: MahouEvalState
m_div = do
  st <- getStack
  case st of
    (MahouInteger b : MahouInteger a : xs) -> putStack $ (MahouInteger $ a `div` b) : xs
    (MahouDouble b : MahouDouble a : xs) -> putStack $ (MahouDouble $ a / b) : xs
    (MahouInteger b : MahouDouble a : xs) -> putStack $ (MahouDouble $ a / (fromIntegral b)) : xs
    (MahouDouble b : MahouInteger a : xs) -> putStack $ (MahouDouble $ (fromIntegral a) / b) : xs
    _ -> pushStack $ MahouError "- div - Invalid arguments!"

m_dup :: MahouEvalState
m_dup = do
  st <- getStack
  case st of
    (a : xs) -> putStack $ a : a : xs
    _ -> pushStack $ MahouError "- dup - Stack is empty!"

m_echo :: MahouEvalState
m_echo = do
  st <- getStack
  case st of
    (MahouString s : xs) -> do
      putStack $ xs
      putHtml s
    _ -> m_asStr >> m_echo

m_equal :: MahouEvalState
m_equal = do
  st <- getStack
  case st of
    (b : a : xs) -> do
      if a == b then
        putStack $ (MahouInteger 1) : xs
      else
        putStack $ (MahouInteger 0) : xs
    _ -> pushStack $ MahouError "- equal - Invalid arguments!"

m_eval :: MahouEvalState
m_eval = do
  st <- getStack
  case st of
    (MahouList l : xs) -> do
      eval l
    _ -> pushStack $ MahouError "- eval - Invalid arguments!"

m_explode :: MahouEvalState
m_explode = do
  st <- getStack
  case st of
    (MahouString s : xs) -> do
      putStack $ (MahouList $ Prelude.map (MahouCharacter) s) : xs
    (MahouInteger i : xs) -> do
      putStack $
          (MahouList $ map (\s -> MahouInteger $ ((read [s]) :: Integer)) (show . abs $ i)) : xs
    _ -> pushStack $ MahouError "- explode - Invalid arguments!"

m_filter :: MahouEvalState
m_filter = do
  st <- getStack
  case st of
    (MahouList f : MahouList ls : xs) -> do
      m <- filter' f ls
      putStack $ (MahouList m) : xs
    (MahouList f : MahouString ls : xs) -> do
      m <- filter'' f ls
      putStack $ (MahouString m) : xs
    _ -> pushStack $ MahouError "- filter - Invalid arguments!"
 where filter' f [] = return []
       filter' f (a:as) = do
         pushStack a
         eval f
         st' <- getStack
         case st' of
           (MahouInteger 0 : qs) -> do 
             putStack qs
             m <- filter' f as
             return m
           (q : qs) -> do
             putStack qs
             m <- filter' f as
             return $ a : m
       filter'' f [] = return []
       filter'' f (a:as) = do
         pushStack (MahouCharacter a)
         eval f
         st' <- getStack
         case st' of
           (MahouInteger 0 : qs) -> do 
             putStack qs
             m <- filter'' f as
             return m
           (q : qs) -> do
             putStack qs
             m <- filter'' f as
             return $ a : m

m_findIndex :: MahouEvalState
m_findIndex = do
  st <- getStack
  case st of
    (MahouList p : MahouList ls : xs) -> do
      putStack xs
      idx <- findIndex' p ls 0
      pushStack . MahouList $ ls
      pushStack . MahouInteger $ idx
    _ -> m_swap >> m_explode >> m_swap >> m_findIndex
 where findIndex' p [] _ = return $ -1
       findIndex' p (x:xs) i = do
         pushStack x
         eval p
         st' <- getStack
         case st' of
           (MahouInteger 1 : ys) -> do
             putStack ys
             return i
           (a : ys) -> do 
             putStack ys
             idx <- findIndex' p xs (succ i)
             return idx
           _ -> return $ -1

m_getElement :: MahouEvalState
m_getElement = do
  st <- getStack
  case st of
    (MahouInteger b : MahouList a : xs) -> do
      if (b >= genericLength a) then
        pushStack $ MahouError "- get-element - Index out of bounds!"
      else
        putStack $ (a !! (toInt b)) : xs
    _ -> m_swap >> m_explode >> m_swap >> m_getElement

m_getCookie :: MahouEvalState
m_getCookie = do
  st <- getStack
  case st of
    (MahouString name : MahouString def : xs) -> do
      ck <- lift $ getCookie name
      case ck of
        (Just q) -> putStack $ (MahouString q) : xs
        Nothing -> putStack $ (MahouString def) : xs
    _ -> pushStack $ MahouError "- get-cookie - Invalid arguments!"

m_getProperty :: MahouEvalState
m_getProperty = do
  st <- getStack
  case st of
    (MahouString name : MahouObject obj : xs) -> do
      putStack $ (MahouObject obj) : xs
      case Map.lookup name obj of
        Just q -> eval q
        _ -> pushStack $ MahouError "- get-property - Property not found!"
    _ -> pushStack $ MahouError "- get-property - Invalid arguments!"

m_getQueryURI :: MahouEvalState
m_getQueryURI = do
  u <- lift $ queryURI
  let a' = uriAuthority u
  let a = case a' of
            (Just q) -> MahouList [ MahouString $ uriUserInfo q,
                                    MahouString $ uriRegName q,
                                    MahouString $ uriPort q]
            _ -> MahouList []
  pushStack $ MahouList [ MahouString $ uriScheme u, 
                          a,
                          MahouString $ uriPath u,
                          MahouString $ uriQuery u,
                          MahouString $ uriFragment u]

m_head :: MahouEvalState
m_head = do
  st <- getStack
  case st of
    (MahouList (a : as) : xs) -> do
      putStack $ a :xs
    _ -> m_explode >> m_head

m_isSpace :: MahouEvalState
m_isSpace = do
  st <- getStack
  case st of
    (MahouCharacter c : xs) -> do
      if isSpace c then
        putStack $ (MahouInteger 1) : xs
      else
        putStack $ (MahouInteger 0) : xs
    _ -> pushStack $ MahouError "- is-space - Invalid arguments!"

m_isNumber :: MahouEvalState
m_isNumber = do
  st <- getStack
  case st of
    (MahouCharacter c : xs) -> do
      if isNumber c then
        putStack $ (MahouInteger 1) : xs
      else
        putStack $ (MahouInteger 0) : xs
    _ -> pushStack $ MahouError "- is-number - Invalid arguments!"

m_isAlphaNum :: MahouEvalState
m_isAlphaNum = do
  st <- getStack
  case st of
    (MahouCharacter c : xs) -> do
      if isAlphaNum c then
        putStack $ (MahouInteger 1) : xs
      else
        putStack $ (MahouInteger 0) : xs
    _ -> pushStack $ MahouError "- is-alpha-num - Invalid arguments!"

m_isAlpha :: MahouEvalState
m_isAlpha = do
  st <- getStack
  case st of
    (MahouCharacter c : xs) -> do
      if isAlpha c then
        putStack $ (MahouInteger 1) : xs
      else
        putStack $ (MahouInteger 0) : xs
    _ -> pushStack $ MahouError "- is-alpha - Invalid arguments!"

m_length :: MahouEvalState
m_length = do
  st <- getStack
  case st of
    (MahouList l : xs) -> do
      putStack $ (MahouInteger . fromIntegral $ length l) : xs
    _ -> m_explode >> m_length

m_map :: MahouEvalState
m_map = do
  st <- getStack
  case st of
    (MahouList f : MahouList ls : xs) -> do
      putStack xs
      m <- map' f ls
      pushStack . MahouList $ m
    _ -> m_swap >> m_explode >> m_swap >> m_map
 where map' _ [] = return []
       map' f (x:xs) = do
         pushStack x
         eval f
         st' <- getStack
         case st' of
           (a : ys) -> do
             putStack ys
             m <- map' f xs
             return $ a : m
           _ -> return $ [MahouError "- map - Stack error!"]

m_mod :: MahouEvalState
m_mod = do
  st <- getStack
  case st of
    (MahouInteger b : MahouInteger a : xs) -> putStack $ (MahouInteger $ a `mod` b) : xs
    (MahouDouble b : MahouDouble a : xs) -> putStack $ (MahouInteger $ (floor a) `mod` (floor b)) : xs
    (MahouInteger b : MahouDouble a : xs) -> putStack $ (MahouInteger $ (floor a) `mod`  b) : xs
    (MahouDouble b : MahouInteger a : xs) -> putStack $ (MahouInteger $ a `mod` (floor b)) : xs
    _ -> pushStack $ MahouError "- mod - Invalid arguments!"

m_mul :: MahouEvalState
m_mul = do
  st <- getStack
  case st of
    (MahouInteger b : MahouInteger a : xs) -> putStack $ (MahouInteger $ a * b) : xs
    (MahouDouble b : MahouDouble a : xs) -> putStack $ (MahouDouble $ a * b) : xs
    (MahouInteger b : MahouDouble a : xs) -> putStack $ (MahouDouble $ a * (fromIntegral b)) : xs
    (MahouDouble b : MahouInteger a : xs) -> putStack $ (MahouDouble $ (fromIntegral a) * b) : xs
    _ -> pushStack $ MahouError "- mul - Invalid arguments!"

m_not :: MahouEvalState
m_not = do
  st <- getStack
  case st of
    (MahouInteger 0 : xs) -> putStack $ (MahouInteger 1) : xs
    (a : xs) -> putStack $ (MahouInteger 0) : xs
    _ -> pushStack $ MahouError "- not - Invalid arguments!"

m_pop :: MahouEvalState
m_pop = do
  st <- getStack
  case st of
    (a : xs) -> putStack xs
    _ -> pushStack $ MahouError "- pop - Stack is empty!"

m_prepend :: MahouEvalState
m_prepend = do
  st <- getStack
  case st of
    (b : MahouList a : xs) -> putStack $ (MahouList $ b : a) : xs
    (MahouCharacter b : MahouString a : xs) -> putStack $ (MahouString $ b : a) : xs
    (MahouString b : MahouCharacter a : xs) -> putStack $ (MahouString $ b ++ [a]) : xs
    _ -> m_swap >> m_cnat

m_putStr :: MahouEvalState
m_putStr = do
  st <- getStack
  case st of
    (MahouString a : xs) -> do
      liftIO $ putStr a
      putStack xs
    _ -> m_asStr >> m_putStr

m_putStrLn :: MahouEvalState
m_putStrLn = do
  st <- getStack
  case st of
    (MahouString a : xs) -> do
      liftIO $ putStr a
      liftIO $ putStrLn ""
      putStack xs
    _ -> m_asStr >> m_putStr >> (liftIO $ putStrLn "")

m_range :: MahouEvalState
m_range = do
  st <- getStack
  case st of
    (MahouInteger b : MahouInteger a : xs) -> putStack $ (MahouList . map MahouInteger $ [a..b]) : xs
    (MahouCharacter b : MahouCharacter a : xs) -> putStack $ (MahouString [a..b]) : xs

m_readFile :: MahouEvalState
m_readFile = do
  st <- getStack
  case st of
    (MahouString fp : xs) -> do
      txt <- liftIO $ readFile fp
      putStack $ (MahouString txt) : xs
    _ -> pushStack $ MahouError "- read-file - Invalid arguments!"

m_readInt :: MahouEvalState
m_readInt = do
  st <- getStack
  case st of
    (MahouString a : xs) -> do
      case (reads a) :: [(Integer, String)] of
        [(i,_)] -> putStack $ (MahouInteger i) : xs
        _ -> pushStack $ MahouError "- read-int - Could not read Integer!"
    _ -> pushStack $ MahouError "- read-int - Invalid arguments!"
    
m_popl :: MahouEvalState
m_popl = do
  (a, b, c, t) <- get
  case t of
    (t:ts) -> put (a, b, c, ts) >> pushStack t
    _ -> pushStack $ MahouError "- popl - Stack empty!"
    
m_peekl :: MahouEvalState
m_peekl = do
  (a, b, c, t) <- get
  case t of
    (t:ts) -> put (a, b, c, t:ts) >> pushStack t
    _ -> pushStack $ MahouError "- peekl - Stack empty!"

m_reduce :: MahouEvalState
m_reduce = do
  st <- getStack
  case st of
    (MahouList f : MahouString s : xs) -> do
      m_swap
      m_explode
      m_swap
      m_reduce
    (MahouList f : MahouList ls : xs) -> do
      putStack xs
      reduce' f ls
 where reduce' f [] = pushStack $ MahouError "- reduce - Empty list!"
       reduce' f (x: xs) = reduce'' f x xs
       reduce'' f z [] = pushStack z
       reduce'' f z (x:xs) = do
         pushStack z
         pushStack x
         eval f
         st' <- getStack
         case st' of
           (a : ys) -> do 
             putStack ys
             reduce'' f a xs
           _ -> pushStack $ MahouError "- reduce - Stack error!" 

m_replicate :: MahouEvalState
m_replicate = do
  st <- getStack
  case st of
    (MahouInteger i : a : xs) -> do
      putStack $ (MahouList (replicate (toInt i) a)) : xs
    _ -> pushStack $ MahouError "- replicate - Invalid arguments!"

m_reverse :: MahouEvalState
m_reverse = do
  st <- getStack
  case st of
    (MahouString val : xs) -> do
      putStack $ (MahouString . reverse $ val) : xs
    (MahouList val : xs) -> do
      putStack $ (MahouList . reverse $ val) : xs
    _ -> pushStack $ MahouError "- reverse - Invalid arguments!"

m_rotateStack :: MahouEvalState
m_rotateStack = do
  st <- getStack
  case st of
    (a : xs) -> do
      putStack $ xs ++ [a]
    _ -> return ()

m_rotateStack2 :: MahouEvalState
m_rotateStack2 = do
  st <- getStack
  case st of
    (a : xs) -> do
      putStack $ (last xs) : a : (init xs)
    _ -> return ()

m_rotateStackTop :: MahouEvalState
m_rotateStackTop = do
  st <- getStack
  case st of
    (MahouInteger n : xs) -> do
      let (t:ts) = genericTake n xs
      let bottom = genericDrop n xs
      putStack $ (ts ++ [t]) ++ bottom
    _ -> pushStack $ MahouError "- rotate-stack-top - Invalid arguments!"

m_rotateStackTop2 :: MahouEvalState
m_rotateStackTop2 = do
  st <- getStack
  case st of
    (MahouInteger n : xs) -> do
      let (t:ts) = genericTake n xs
      let bottom = genericDrop n xs
      putStack $ ((last ts) : t : (init ts)) ++ bottom
    _ -> pushStack $ MahouError "- rotate-stack-top - Invalid arguments!"
    
    
m_pushl :: MahouEvalState
m_pushl = do
  st <- getStack
  case st of
    (n : xs) -> do
      (a, b, c, t) <- get
      put (a, b, c, n : t)
      putStack xs
    _ -> pushStack $ MahouError "- pushl - Stack empty!"

m_setCookie :: MahouEvalState
m_setCookie = do
  st <- getStack
  case st of
    (MahouString val : MahouString name : xs) -> do
      putStack xs
      lift $ setCookie (newCookie name val)
    _ -> pushStack $ MahouError "- set-cookie - Invalid arguments!"

m_setProperty :: MahouEvalState
m_setProperty = do
  st <- getStack
  case st of
    (val : MahouString name : MahouObject obj : xs) -> do
      putStack $ (MahouObject (Map.insert name [val] obj) : xs) 
    _ -> pushStack $ MahouError "- set-property - Invalid arguments!"

m_show :: MahouEvalState
m_show = do
  st <- getStack
  case st of 
    (d : xs) -> do 
      liftIO . putStr . stdStr $ d
      putStack $ xs
    _ -> pushStack $ MahouError "- show - Invalid arguments!"

m_showLn :: MahouEvalState
m_showLn = do
  m_show
  liftIO $ putStr "\n"

m_signum :: MahouEvalState
m_signum = do
  st <- getStack
  case st of
    (MahouInteger a : xs) -> putStack $ (MahouInteger $ signum a) : xs
    (MahouDouble a : xs) -> putStack $ (MahouDouble $ signum a) : xs
    _ -> pushStack $ MahouError "- signum - Invalid arguments!"

m_sub :: MahouEvalState
m_sub = do
  st <- getStack
  case st of
    (MahouInteger b : MahouInteger a : xs) -> putStack $ (MahouInteger $ a - b) : xs
    (MahouDouble b : MahouDouble a : xs) -> putStack $ (MahouDouble $ a - b) : xs
    (MahouInteger b : MahouDouble a : xs) -> putStack $ (MahouDouble $ a - (fromIntegral b)) : xs
    (MahouDouble b : MahouInteger a : xs) -> putStack $ (MahouDouble $ (fromIntegral a) - b) : xs
    _ -> pushStack $ MahouError "- sub - Invalid arguments!"

m_swap :: MahouEvalState
m_swap = do
  st <- getStack
  case st of
    (b : a : xs) -> putStack $ a : b : xs
    _ -> pushStack $ MahouError "- swap - Invalid arguments!"

m_tail :: MahouEvalState
m_tail = do
  st <- getStack
  case st of
    (MahouList (a : as) : xs) -> do
      putStack $ (MahouList as) :xs
    _ -> m_explode >> m_tail
    
m_take :: MahouEvalState
m_take = do
  st <- getStack
  case st of
    (MahouList a : MahouInteger b : xs) -> putStack $ (MahouList (take' (toInt b) a)) : xs
    (MahouInteger b : MahouList a : xs) -> putStack $ (MahouList (take' (toInt b) a)) : xs
    (MahouString a : MahouInteger b : xs) -> putStack $ (MahouString (take' (toInt b) a)) : xs
    (MahouInteger b : MahouString a : xs) -> putStack $ (MahouString (take' (toInt b) a)) : xs
    _ -> pushStack $ MahouError "- take - Invalid arguments!"
  where take' n xs
          |n < 0 = xs
          |otherwise = take n xs          
 
m_drop :: MahouEvalState
m_drop = do
  st <- getStack
  case st of
    (MahouList a : MahouInteger b : xs) -> putStack $ (MahouList (drop' (toInt b) a)) : xs
    (MahouInteger b : MahouList a : xs) -> putStack $ (MahouList (drop' (toInt b) a)) : xs
    (MahouString a : MahouInteger b : xs) -> putStack $ (MahouString (drop' (toInt b) a)) : xs
    (MahouInteger b : MahouString a : xs) -> putStack $ (MahouString (drop' (toInt b) a)) : xs
  where drop' n xs
          |n < 0 = []
          |otherwise = drop n xs

m_uncons :: MahouEvalState
m_uncons = do
  st <- getStack
  case st of
    (MahouList (a : as) : xs) -> do
      putStack $ a : MahouList as : xs
    _ -> m_explode >> m_uncons

    