module Mahou.Parser
  (runParserWithString,
   parseData,
   parseCode)
 where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Combinator
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Token

import Mahou.Types
import Mahou.Helpers
import Debug.Trace
import Data.Map as Map hiding (null)

parseDouble :: Parser MahouExpression
parseDouble = do 
  s <- many $ char '-'
  n1 <- many1 digit
  char '.'
  n2 <- many1 digit
  optional spaces
  if not.null $ s then 
    return $ MahouDouble (-1 * (read (n1 ++ "." ++ n2)))
  else return $ MahouDouble (read (n1 ++ "." ++ n2))

parseInteger :: Parser MahouExpression
parseInteger = do 
  s <- many $ char '-'
  num <- many1 digit
  optional spaces
  if not.null $ s then
    return $ MahouInteger (-1 * (read num))
  else return $ MahouInteger (read num)

parseList :: Parser MahouExpression
parseList = do char '['
               optional spaces
               t <- parseList'
               optional spaces
               return $ MahouList t
  where parseList' :: Parser [MahouExpression]
        parseList' = do char ']'
                        return []
                     <|>
                     do n <- parseData
                        optional spaces
                        many $ oneOf ", \t"
                        (do t <- parseList'
                            optional spaces
                            return (n : t))
                         <|>
                         do char ']'
                            optional spaces
                            return [n]

parseOperator :: Parser MahouExpression
parseOperator = do
  char ':'
  name <- many1 . oneOf $ ichars
  optional spaces
  return $ MahouOperator name

parseVerb :: Parser MahouExpression
parseVerb = do
 name <- many1 . oneOf $ ichars
 optional spaces
 return $ MahouVerb name
 
parseMemberGet :: Parser MahouExpression
parseMemberGet = do
 string "|:"
 optional spaces
 name <- many1 . oneOf $ ichars
 optional spaces
 return $ MahouMemberGet name
 
parseTaggedVerb :: Parser MahouExpression
parseTaggedVerb = do
 string "_"
 optional spaces
 name <- many1 . oneOf $ ichars
 optional spaces
 return $ MahouTaggedVerb name
 
parseMemberSet :: Parser MahouExpression
parseMemberSet = do
 string "|="
 name <- many1 . oneOf $ ichars
 optional spaces
 return $ MahouMemberSet name

parseString :: Parser MahouExpression
parseString = do 
  _ <- char '"'
  e <- many (noneOf "\"")
  _ <- char '"'
  optional spaces
  return $ MahouString (unescape e)

parseChar :: Parser MahouExpression
parseChar = do
  char '\''
  e <- anyChar
  optional spaces
  return $ MahouCharacter e

parseData :: Parser MahouExpression
parseData = (try parseDouble) <|> 
            (try parseInteger) <|>
            (try parseOperator) <|>
            (try parseHtml) <|>
            (try parseObject) <|>
            (try parseLock) <|>
            (try parseVerb) <|>
			(try parseMemberGet) <|>
			(try parseMemberSet) <|>
            (try parseTaggedVerb) <|>
            parseString <|>
            parseChar <|>
            parseComment <|>
            parseBlockComment <|>
            parseList

ichars = ['a'..'z'] ++ ['A'..'Z'] ++ "+*-/%$?!@~^<>."

parseDef :: Parser MahouExpression
parseDef = do
  optional spaces
  string "def"
  optional spaces
  name <- many1 $ oneOf ichars
  optional spaces
  char '{'
  optional spaces
  code <- many $ parseData
  optional spaces
  char '}'
  optional spaces
  return $ MahouDef name code

parseMain :: Parser MahouExpression
parseMain = do
  string ";"
  optional $ char '{'
  code <- many $ parseData
  optional spaces
  char '}'
  optional spaces
  return $ MahouDef "main" code

parseHtml :: Parser MahouExpression
parseHtml = do
  string "html"
  char '{'
  code <- many $ noneOf "}"
  optional spaces
  char '}'
  optional spaces
  return $ MahouString (unescape2 code)

parseObject :: Parser MahouExpression
parseObject = do
  string "obj"
  char '{'
  optional spaces
  ls <- many $ parseProperty
  optional spaces
  char '}'
  optional spaces
  return $ MahouObject (Map.fromList ls)
 where parseProperty :: Parser (String, [MahouExpression])
       parseProperty = do
         name <- many1 $ oneOf ichars
         optional spaces
         char '='
         optional spaces
         code <- many $ parseData
         optional spaces
         char ','
         optional spaces
         return (name, code)

parseLock :: Parser MahouExpression
parseLock = do
  string "lock"
  char '{'
  optional spaces
  ls <- many1 $ do 
     optional spaces
     q <- many $ parseData
     optional spaces
     char ','
     optional spaces
     return q
  optional spaces
  char '}'
  optional spaces
  return $ MahouLock ls

parseComment :: Parser MahouExpression
parseComment = do
  oneOf "#"
  optional spaces
  many $ noneOf "\n"
  optional spaces
  return $ MahouVerb "nop"
  
parseBlockComment :: Parser MahouExpression
parseBlockComment = do
  string "/*"
  many $ noneOf "*/"
  string "*/"
  optional spaces
  return $ MahouVerb "nop"
   
parseCode :: Parser [MahouExpression]
parseCode = do
  defs <- many1 $ parseMain <|> parseDef <|> parseComment <|> parseBlockComment
  return defs

runParserWithString p input = 
  case parse p "" input of
    Left err -> error $ show err
    Right q -> q
