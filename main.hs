import Mahou.Parser
import Mahou.Eval
import Mahou.Types

import Data.Map
import System.Environment
import Network.CGI.Monad
import System.IO
import Data.IORef
import Data.ByteString.Lazy.Char8 (pack, ByteString)

{- 
  Goals
  ------

  - Easy to use language with a ridicously huge amount of builtins for almost everything.
  - Stack based
  - Dynamic
-}

exec prg = do
  (_, _, h, _) <- execStateT (runMain $  runParserWithString parseCode prg) (MahouVerbs $ empty, [], "", [])
  return h

execStdin prg sin = do
  runStateT (runMain $  runParserWithString parseCode prg) (MahouVerbs $ empty, [], "", [])
  return ()

cgiMain file = do
  prog <- liftIO $ readFile file
  {- std <- liftIO $ readFile "./std/std.mh" -}
  h <- exec (prog {- ++ "\n" ++ std -})
  output h

cgiMain2 xs = do
  code <- liftIO $ (readFiles xs)
  exec (code)
  
readFiles [] = return ""
readFiles (x:xs) = do
 t <- readFile x
 ts <- readFiles xs
 return (t ++ "\n" ++ ts)

main = do
 args <- getArgs
 case args of
   "--cgi" : file : xs -> do
     runCGI $ handleErrors (cgiMain file)
   "--file" : xs -> do
      let req = CGIRequest { cgiVars = fromList [], cgiInputs = [], cgiRequestBody = pack "" }
      runCGIT (cgiMain2 xs) req
      return ()
   "--foo" : xs -> do
      ioref <- newIORef ""
      fooMode ioref
   _ -> writeFile "/tmp/foo.txt" (show args)
   
fooMode ioref = do
  putStr ":) "
  hFlush stdout
  t <- getLine
  pre <- readIORef ioref
  writeIORef ioref (pre ++ "\n" ++ t)
  let req = CGIRequest { cgiVars = fromList [], cgiInputs = [], cgiRequestBody = pack "" }
  runCGIT (exec (pre ++ "\n" ++ t)) req
  fooMode ioref
