# Mahou

## About

Mahou is the working title of my new programming language creation project. It is a stack-based
(web-)script programming language. 

## Help Wanted

### If you know haskell?

... you can help working on the interpreter.

### If you don't know haskell?

Then you can learn Mahou and help extend the standard library or just help me document it.

## Example

CGI:

	#!/usr/bin/mahou --cgi
	
	def main {
	  "0" "visits" get-cookie dup
	  "Hello. This is your visit number: " swap cnat echo "\n" echo
	  read-int 1 add as-str "visits" swap set-cookie
	}

Normal:

	def main {
	   "Index of C in alphabet is: " 
	   lower-alphabet [ 'c equal ] find-index cnat 
	   put-str "\n" put-str
	}
